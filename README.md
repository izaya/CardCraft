# CardCraft
This is an initial prototype of my online card game written in [ReactJS](https://facebook.github.io/react/). Now the game client has been entirely written in Unity as I need more complicated UI and dynamically instantiate NEW game objects like cards drawn from deck which receives data from the game server using OOP rather than render static HTML DOM. So this repository is abandoned. However writting ES6 in React is still an enjoyable experience to me.  
The new project is at [https://gitlab.com/izaya/CardIO](https://gitlab.com/izaya/CardIO)  
You can still run this project by running index.js file and install the dependencies in package.json using npm or yarn.
