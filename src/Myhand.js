/**
 * Created by Francis Yang on 7/4/17.
 */
import React from 'react';
import './Myhand.css';
import Kaado from './Kaado';
/**
 * Player instance
 */
// state 0: online chilling, not my turn
// state 1: my turn to attack
// state 2: I win
// state -1: I lose
// state -2: offline
class Myhand extends React.Component {
	constructor () {
		super ();
		if (false === (this instanceof Myhand)) {
			return new Myhand();
		}
		//this._playerID;
		this._Cards = [];
		this._state = -2;
	}

	renderCards (i) {
		return (
			<Kaado />
		);
	}

	render () {
		let cardsRenderer = [];
		for (let i = 0; i < this._Cards.length; i++) {
			cardsRenderer.push(this.renderCards(i));
		}
		return (
			<div className="myhand">
				{ cardsRenderer }
			</div>
		);
	}
}

Myhand.prototype.setState = function (status) {
	this._state = status;
};

Myhand.prototype.getState = function () {
	return this._state;
};

Myhand.prototype.getCards = function () {
	return this._Cards;
};

Myhand.prototype.insert = function (card) {
	this._Cards.push(card);
};

Myhand.prototype.getSize = function () {
	return this._Cards.length;
};

Myhand.prototype.pop = function () {
	return this._Cards.pop();
};

export  default Myhand;