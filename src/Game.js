/**
 * Created by Francis Yang on 7/4/17.
 */
import React from 'react';
import './Game.css';
import Kaado from './Kaado';
import Myhand from './Myhand';
import {Button} from 'reactstrap';

class Game extends React.Component {
	constructor () {
		super ();
		if (false === (this instanceof Game)) {
			return new Game ();
		}
		// state 0: online chilling, not my turn
		// state 1: my turn to attack
		// state 2: I win
		// state -1: I lose
		// state -2: offline
		this.state = {
			state: 1,
			drawCard: true,
			insertCard: null,
			players: [{
				hand: Array (0)
			}]
		}
		// card deck
		this._cards = [];
		this._cards.push (new Kaado (0, '1s', 1, 1));
		this._cards.push (new Kaado (1, '2s', 2, 1));
		this._cards.push (new Kaado (2, '3s', 1, 2));
		this._cards.push (new Kaado (3, '4s', 2, 2));
		this._cards.push (new Kaado (4, '5s', 3, 1));
		this.shuffleArray(this._cards);
	}

	render () {
		return (
			<div className="game">
				<div className="table">
					<div className="drawcard">
						<Button color = "primary" size = "lg" onClick={ () => {if (this.state.drawCard) {this.drawCard()}} }>Draw Card</Button>
					</div>
				</div>
				<Myhand insert = {this.state.insertCard}/>
			</div>
		);
	}
}

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm or Fisher-Yates (aka Knuth) Shuffle.
 */
Game.prototype.shuffleArray = function (array) {
	for (var i = array.length - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	return array;
};

Game.prototype.drawCard = function() {
	var card = this._cards.pop();
	//this.state.drawCard = false;
	this.state.insertCard = card;
	console.log(card);
	// card.x =  myHand.getSize() * (card.width + 10);
	// card.y = game.height - card.height;
	// Myhand.insert (card);
}

export default Game;