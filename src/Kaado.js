/**
 * Created by Francis Yang on 7/4/17.
 */
import React from 'react';
import './Kaado.css';
import {Card, CardImg, CardText, CardBlock, Button} from 'reactstrap';

/**
 * Card instance, Kaado (カード) means card in Japanese
 */
// state 0: in the deck or hand
// state 1: chilling on the table
// state 2: turn to attack
// state -1: destroy
class Kaado extends React.Component {
	constructor (id, img, health, damage) {
		super ();
		if (false === (this instanceof Kaado)) {
			return new Kaado ();
		}
		this.id = id;
		this.img = img;
		this.health = health;
		this.damage = damage;
	}

	render () {
		return (
			<Card>
				<CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
				<CardBlock>
					<CardText>
						HP: 2<br />
						Damage: 1
					</CardText>
					<Button color = "secondary" size = "sm">Attack</Button>
				</CardBlock>
			</Card>
		)
	}
}

export default Kaado;